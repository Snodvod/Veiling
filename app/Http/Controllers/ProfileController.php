<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function showProfile()
    {
        $user = Auth::user();

        $activeAuctions = [];
        foreach($user->auctionsowner as $auction) {
            if($auction->status == "Active") {
                array_push($activeAuctions, $auction);
            }
        }
        $timeArray = timecalc::calculate($activeAuctions);

        return view('user.profile', ['user' => $user, 'activeAuctions' => $activeAuctions, 'timediff' => $timeArray]);
    }
}
