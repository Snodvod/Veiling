<?php

namespace App\Http\Controllers;

use App\Auction;
use App\Bidder;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class BidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function bid(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'prev' => 'required',
            'bid' => 'required|greater_than_field:prev'
        ]);

        $auction = Auction::findOrFail($request->id);
        $auction->price = $request->bid;
        $auction->save();
        $user = Auth::user();
        $bidder = new Bidder();
        $bidder->user()->associate($user);
        $bidder->auction()->associate($auction);
        $bidder->price = $request->bid;
        $bidder->save();

        return redirect()->back();
    }

    public function buy(Request $request)
    {
        $auction = Auction::findOrFail($request->id);
        $user = Auth::user();
        $user->auctionsbuyer()->save($auction);
        $auction->status = "sold";

        $auction->save();

        Mail::send('emails.sorry', ['user' => $user], function($m) use ($user) {
            $m->from('info@landoretti.com', 'Landoretti');

            $m->to($user->email, $user->name)->subject("Sorry, uw bieding is door iemand uitgekocht");
        });

        return redirect('profile');
    }
}
