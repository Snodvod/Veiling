<?php

namespace App\Http\Controllers;

use App\Faq;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faq = Faq::all();

        return view('faq.index', ['faqs' => $faq]);
    }

    public function search() {
        $query = Input::get('query');
        $result = DB::table('faqs')
            ->where('question', 'like', '%' . $query . '%')
            ->get();

        return view('faq.index', ['faqs' => $result]);
    }
}
